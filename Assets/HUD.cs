﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD : MonoBehaviour {

    public Token myToken;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void Open() {
        this.gameObject.SetActive(true);
    }

    public void Close() {
        //First unequip all cards
        UnequipAll();
        this.gameObject.SetActive(false);
    }

    public void Use() {
        myToken.Use();
    }

    public bool CanEquip(Card card) {
        CardSlot[] mySlots = GetComponentsInChildren<CardSlot>();
        for(int i=0; i < mySlots.Length; i++) {
            if (mySlots[i].validCards.Contains(card.id)) {
                return true;
            }
        }
        return false;
    }

    public bool Equip(Card card) {
        CardSlot[] mySlots = GetComponentsInChildren<CardSlot>();
        for (int i = 0; i < mySlots.Length; i++) {
            if (mySlots[i].validCards.Contains(card.id) && mySlots[i].myCard == null) {
                mySlots[i].Equip(card);
                return true;
            }
        }
        return false;
    }

    public void UnequipAll() {
        CardSlot[] mySlots = GetComponentsInChildren<CardSlot>();
        for (int i = 0; i < mySlots.Length; i++) {
            if (mySlots[i].myCard) {
                mySlots[i].Unequip();
            }
        }
    }

    public CardSlot[] GetCardSlots() {
        return GetComponentsInChildren<CardSlot>();
        
    }

}
