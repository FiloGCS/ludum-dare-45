﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cameraman : MonoBehaviour {

    public float range = 2.0f;

    void Update() {
        Vector3 mousePos = Input.mousePosition;
        mousePos.x = mousePos.x / Screen.width * 2 -1;
        mousePos.y = mousePos.y / Screen.height * 2 -1;

        this.transform.position = new Vector3(mousePos.x, 0, mousePos.y * ((float)Screen.height / (float)Screen.width)) * range;
    }
}
