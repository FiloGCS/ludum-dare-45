﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmitAndDie : MonoBehaviour {

    private void Start() {
        StartCoroutine(WaitAndDie());
    }

    IEnumerator WaitAndDie() {
        yield return new WaitForSeconds(2.0f);
        Destroy(this.gameObject);
    }
}
