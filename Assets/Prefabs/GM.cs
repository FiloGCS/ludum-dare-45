﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM : MonoBehaviour {

    public static Camera camera;
    public static GM gm;
    public GameObject dieEffectPrefab;
    public static float timeScale = 1.0f;
    public List<GameObject> cardGameObjects;
    public List<Material> cardMaterials;
    public List<Token> tokenGameObjects;

    private int state = 0;
    private List<int> compTokens;

    private void Awake() {
        gm = this;
        timeScale = 1.0f;
    }

    private void Start() {
        camera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    private void Update() {
        switch (state) {
            case 0:
                //Esperando a que el jugador tenga la primera idea
                foreach(Card card in Card.Cards) {
                    if (card.id == "idea") {
                        //Empieza el tiempo
                        Instantiate(tokenGameObjects[5], new Vector3(1.5f, 0.5f, 2.0f), Quaternion.identity);
                        //Se da el token de diseño
                        Instantiate(tokenGameObjects[1], new Vector3(-1.5f, 0.5f, -1f), Quaternion.identity);
                        state++;
                        break;
                    }
                }
                break;
            case 1:
                //Esperando a que el jugador tenga el primer componente de mecanicas
                foreach(Card card in Card.Cards) {
                    if(card.id == "mechanic") {
                        //Se da el token de codigo
                        Instantiate(tokenGameObjects[3], new Vector3(0, 0.5f, -1f), Quaternion.identity);

                        state++;
                        break;
                    }
                }
                break;
            case 2:
                //Esperando al primer code
                foreach (Card card in Card.Cards) {
                    if (card.id == "code") {
                        //Se da el token de dibujar
                        Instantiate(tokenGameObjects[2], new Vector3(1.5f, 0.5f, -1f), Quaternion.identity);

                        state++;
                        break;
                    }
                }
                break;
            case 3:
                //Esperando ART
                foreach (Card card in Card.Cards) {
                    if (card.id == "art") {
                        //Se da el token de buildear
                        Instantiate(tokenGameObjects[4], new Vector3(1.5f, 0.5f, 0.50f), Quaternion.identity);

                        state++;
                        break;
                    }
                }
                break;

            case 4:
                //Esperando a que el jugador tenga el primer juego
                foreach (Card card in Card.Cards) {
                    if (card.id == "game") {
                        //Se da el token de buildear
                        Instantiate(tokenGameObjects[6], new Vector3(-1.5f, 0.5f, 0.5f), Quaternion.identity);

                        state++;
                        break;
                    }
                }
                break;

            default:
                break;
        }
    }

}
