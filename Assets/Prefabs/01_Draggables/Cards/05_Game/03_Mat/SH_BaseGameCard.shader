// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "custom/GameCard"
{
	Properties
	{
		_YellowTex("YellowTex", 2D) = "white" {}
		_RedTex("RedTex", 2D) = "white" {}
		_GreenTex("GreenTex", 2D) = "white" {}
		_BlueTex("BlueTex", 2D) = "white" {}
		_EmissiveStrength("Emissive Strength", Range( 0 , 3)) = 0
		_Color("Color", Color) = (1,1,1,0)
		_N("N", Range( 1 , 999)) = 1
		_Yellow("Yellow", Float) = 0
		_Red("Red", Float) = 0
		_Green("Green", Float) = 0
		_Blue("Blue", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color;
		uniform float _EmissiveStrength;
		uniform float _Yellow;
		uniform sampler2D _YellowTex;
		uniform float4 _YellowTex_ST;
		uniform float _Red;
		uniform sampler2D _RedTex;
		uniform float4 _RedTex_ST;
		uniform float _Green;
		uniform sampler2D _GreenTex;
		uniform float4 _GreenTex_ST;
		uniform float _Blue;
		uniform sampler2D _BlueTex;
		uniform float4 _BlueTex_ST;
		uniform float _N;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 uv_YellowTex = i.uv_texcoord * _YellowTex_ST.xy + _YellowTex_ST.zw;
			float2 uv_RedTex = i.uv_texcoord * _RedTex_ST.xy + _RedTex_ST.zw;
			float2 uv_GreenTex = i.uv_texcoord * _GreenTex_ST.xy + _GreenTex_ST.zw;
			float2 uv_BlueTex = i.uv_texcoord * _BlueTex_ST.xy + _BlueTex_ST.zw;
			o.Emission = ( _Color * ( _EmissiveStrength * ( ( ( _Yellow * tex2D( _YellowTex, uv_YellowTex ) ) + ( _Red * tex2D( _RedTex, uv_RedTex ) ) + ( _Green * tex2D( _GreenTex, uv_GreenTex ) ) + ( _Blue * tex2D( _BlueTex, uv_BlueTex ) ) ) / _N ) ) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
208;73;1241;649;1083.27;305.306;1.5192;True;False
Node;AmplifyShaderEditor.CommentaryNode;24;-1455.027,-161.0069;Float;False;811.5283;379.2957;Yellow;3;2;17;18;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;23;-1453.777,226.0979;Float;False;811.528;379.2957;Red;3;22;20;21;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;25;-1463.208,620.9877;Float;False;811.528;379.2957;Green;3;29;28;27;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;30;-1452.103,1006.398;Float;False;811.528;379.2957;Blue;3;33;32;31;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;2;-1120.028,-11.71118;Float;True;Property;_YellowTex;YellowTex;0;0;Create;True;0;0;False;0;1d430c808b28fab409f8755dd8d0f77f;1d430c808b28fab409f8755dd8d0f77f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;18;-1009.22,-111.0069;Float;False;Property;_Yellow;Yellow;7;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-1007.97,276.0979;Float;False;Property;_Red;Red;8;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;31;-1117.103,1155.694;Float;True;Property;_BlueTex;BlueTex;3;0;Create;True;0;0;False;0;1d430c808b28fab409f8755dd8d0f77f;1d430c808b28fab409f8755dd8d0f77f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;32;-1006.296,1056.398;Float;False;Property;_Blue;Blue;10;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;20;-1118.777,375.3936;Float;True;Property;_RedTex;RedTex;1;0;Create;True;0;0;False;0;1d430c808b28fab409f8755dd8d0f77f;1d430c808b28fab409f8755dd8d0f77f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;28;-1128.208,770.2834;Float;True;Property;_GreenTex;GreenTex;2;0;Create;True;0;0;False;0;1d430c808b28fab409f8755dd8d0f77f;1d430c808b28fab409f8755dd8d0f77f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;29;-1017.401,670.9877;Float;False;Property;_Green;Green;9;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;-809.5742,1093.366;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;27;-820.6794,707.9554;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-811.2486,313.0655;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-812.4989,-74.03923;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;13;-525.7606,484.3889;Float;False;4;4;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-369.6644,575.4502;Float;False;Property;_N;N;6;0;Create;True;0;0;False;0;1;0;1;999;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;4;-86.89282,78.9723;Float;False;Property;_EmissiveStrength;Emissive Strength;4;0;Create;True;0;0;False;0;0;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;15;-43.81081,475.204;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;237.1072,159.9723;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;6;225.8024,-59.18361;Float;False;Property;_Color;Color;5;0;Create;True;0;0;False;0;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;5;518.8024,112.8164;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;733.7999,119.9768;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;custom/GameCard;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;33;0;32;0
WireConnection;33;1;31;0
WireConnection;27;0;29;0
WireConnection;27;1;28;0
WireConnection;22;0;21;0
WireConnection;22;1;20;0
WireConnection;17;0;18;0
WireConnection;17;1;2;0
WireConnection;13;0;17;0
WireConnection;13;1;22;0
WireConnection;13;2;27;0
WireConnection;13;3;33;0
WireConnection;15;0;13;0
WireConnection;15;1;16;0
WireConnection;3;0;4;0
WireConnection;3;1;15;0
WireConnection;5;0;6;0
WireConnection;5;1;3;0
WireConnection;0;2;5;0
ASEEND*/
//CHKSM=F1B1B1472BAA622B838C804A2757584798747CD4