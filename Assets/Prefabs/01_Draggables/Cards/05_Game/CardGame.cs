﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CardGame : Card {

    public TextMeshProUGUI mechanicText;
    public TextMeshProUGUI artText;
    public TextMeshProUGUI codeText;
    public int[] colors = { 0, 0, 0, 0 };

    public int mechanicPoints = 0;
    public int artPoints = 0;
    public int codePoints = 0;
    public float purity = 1;

    public override void Start() {
        base.Start();
        UpdateValues();
    }

    public void UpdateValues() {
        mechanicText.text = mechanicPoints.ToString();
        artText.text = artPoints.ToString();
        codeText.text = codePoints.ToString();
        //Change the material to this card color
        this.GetComponent<Renderer>().material.SetFloat("_Yellow", colors[0]);
        this.GetComponent<Renderer>().material.SetFloat("_Red", colors[1]);
        this.GetComponent<Renderer>().material.SetFloat("_Green", colors[2]);
        this.GetComponent<Renderer>().material.SetFloat("_Blue", colors[3]);
        int acum = 0;
        for(int i=0; i < colors.Length; i++) {
            acum += colors[i];
        }
        this.GetComponent<Renderer>().material.SetFloat("_N", acum);
    }

    public void AddCard(Card mergedCard) {
        //Add the points of the merged card
        switch (mergedCard.id) {
            case "mechanic":
                mechanicPoints += 1;
                break;
            case "art":
                artPoints += 1;
                break;
            case "code":
                codePoints += 1;
                break;
            default:
                break;
        }
        //Add the color of the merged card
        colors[(int)mergedCard.cardColor]++;

        UpdateValues();
    }
}
