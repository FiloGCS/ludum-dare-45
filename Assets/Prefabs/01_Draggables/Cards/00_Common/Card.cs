﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardColor {Yellow, Red, Green, Blue, Grey, Count};

public class Card : Draggable {


    public static List<Card> Cards = new List<Card>();

    public string id;
    public CardColor cardColor;
    public GameObject ShiningElement;


    private void Awake() {
        Card.Cards.Add(this);
    }

    public override void Drag(Vector3 hitPosition) {
        base.Drag(hitPosition);
        if (transform.parent) {
            transform.parent.GetComponent<CardSlot>().Unequip();
        }

        //Shine all the Tokens that can use this card
        foreach(Token token in Token.Tokens) {
            if (token.CanEquip(this)) {
                token.SetShining(true);
            }
        }
    }

    public override void Drop() {
        base.Drop();
        //Check if I've been dropped on a Token
        Ray ray = GM.camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        LayerMask mask = LayerMask.GetMask("Tokens", "CardSlots");
        //Physics Raycast to grab the element
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask)) {
            //Si me han soltado sobre un token
            if (hit.collider.GetComponent<Token>()) {
                hit.collider.GetComponent<Token>().Equip(this);
            }
            //Si me han soltado sobre un slot
            if (hit.collider.GetComponent<CardSlot>()) {
                hit.collider.GetComponent<CardSlot>().Equip(this);
            }
        }

        //Stop Shining all the Tokens
        foreach (Token token in Token.Tokens) {
            token.SetShining(false);
        }
    }

    public override void SetShining(bool isShining) {
        base.SetShining(isShining);
        if (ShiningElement) {
            ShiningElement.SetActive(isShining);
        }
    }

    public void SetColor(CardColor newColor) {
        cardColor = newColor;
        UpdateColor();
    }

    public void UpdateColor() {
        this.GetComponent<Renderer>().material = GM.gm.cardMaterials[(int)cardColor];
    }
    
    private void OnDestroy() {
        Instantiate(GM.gm.dieEffectPrefab, this.transform.position, this.transform.rotation);
        Card.Cards.Remove(this);
    }

}
