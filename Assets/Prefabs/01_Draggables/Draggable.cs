﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour {

    public GameObject ShadowPrefab;
    [HideInInspector]
    public GameObject shadow;

    private bool beingDragged = false;
    private Vector3 dragOffset;

    public virtual void Awake() {

    }

    public virtual void Start() {

    }

    public void Update() {
        if (beingDragged) {
            this.transform.rotation = Quaternion.identity;
            Ray ray = GM.camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            LayerMask mask = LayerMask.GetMask("Table", "HUD");
            //Physics Raycast to grab the element
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask)) {
                this.transform.position = hit.point + Vector3.up * 0.33f + dragOffset;
            }
        }
    }

    public virtual void Click() {
    }

    public virtual void Drag(Vector3 hitPosition) {
        beingDragged = true;
        dragOffset = this.transform.position - hitPosition;
        if (ShadowPrefab) {
            shadow = (GameObject)Instantiate(ShadowPrefab, this.transform);
        }
    }

    public virtual void Drop() {
        beingDragged = false;
        dragOffset = Vector3.zero;
        if (shadow) {
            Destroy(shadow);
        }
    }

    public virtual void SetShining(bool isShining) {

    }

}
