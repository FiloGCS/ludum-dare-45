﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenThink : Token {

    public override bool CanUse() {
        return true;
    }

    private CardColor ideaColor;
    public override void Use() {
        if (CanUse()) {
            CardSlot[] slots = myHUD.GetCardSlots();
            if (slots[0].myCard) {
                ideaColor = slots[0].myCard.cardColor;
                tokenTimeMultiplier = 3.0f;
            } else {
                ideaColor = (CardColor)Random.Range(0, (int)CardColor.Count - 1);
                tokenTimeMultiplier = 1.0f;
            }
            base.Use();
        }
    }

    public override void OnComplete() {
        base.OnComplete();
        //Spawn an idea card
        GameObject newCard = (GameObject)Instantiate(GM.gm.cardGameObjects[0], this.transform.position + Vector3.up * 0.5f, Quaternion.identity);
        newCard.GetComponent<Card>().SetColor(ideaColor);

    }


}
