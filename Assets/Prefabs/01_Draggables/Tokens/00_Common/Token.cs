﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Token : Draggable {
    public static List<Token> Tokens = new List<Token>();

    public GameObject HUDPrefab;
    public GameObject ShiningElement;
    public TextMeshProUGUI TimerText;
    [HideInInspector]
    public HUD myHUD;

    public float useTime;
    [HideInInspector]
    public float remainingTime = -1;
    public float tokenTimeMultiplier = 1.0f;
    public float tokenSpeed = 1.0f;

    private void Awake() {
        base.Awake();
        Token.Tokens.Add(this);

        //Create HUD
        GameObject newHUD = (GameObject)Instantiate(HUDPrefab, this.transform.position + Vector3.up * 0.5f, Quaternion.Euler(new Vector3(90, 0, 0)));
        myHUD = newHUD.GetComponent<HUD>();
        myHUD.myToken = this;
        CloseHUD();
    }

    public virtual void Start() {

    }

    public virtual void Update() {
        base.Update();
        if (remainingTime > 0) {
            remainingTime -= Time.deltaTime * tokenSpeed * GM.timeScale;
            TimerText.text = remainingTime.ToString("0.0");
            //Did we finish the countdown?
            if (remainingTime <= 0) {
                OnComplete();
                SetCountdownMode(false);
            }
        }
    }

    public virtual void Equip(Card card) {
        if (CanEquip(card)) {
            OpenHUD();
            myHUD.Equip(card);
        }
    }

    public virtual void Use() {
        this.remainingTime = useTime * tokenTimeMultiplier;
        SetCountdownMode(true);

    }

    public virtual void OnComplete() {

    }

    public virtual bool CanUse() {
        return true;
    }

    public virtual bool CanEquip(Card card) {
        return (myHUD.CanEquip(card) && remainingTime<=0);
    }

    public override void Click() {
        if (remainingTime <= 0) {
            base.Click();
            OpenHUD();
        }
    }

    public void OpenHUD() {
        if (!myHUD.isActiveAndEnabled) {
            myHUD.transform.position = this.transform.position + Vector3.up * 0.25f;
            myHUD.Open();
        }
    }

    public void CloseHUD() {
        if (myHUD.isActiveAndEnabled) {
            myHUD.Close();
        }
    }

    public override void SetShining(bool isShining) {
        base.SetShining(isShining);
        ShiningElement.SetActive(isShining);
    }

    public void SetCountdownMode(bool b) {
        if (b) {
            TimerText.gameObject.SetActive(true);
            this.GetComponent<Renderer>().material.SetFloat("_EmissiveStrength", 0.6f);
            CloseHUD();
        } else {
            TimerText.gameObject.SetActive(false);
            this.GetComponent<Renderer>().material.SetFloat("_EmissiveStrength", 1f);

        }
    }

    private void OnDestroy() {
        Token.Tokens.Remove(this);
    }


}
