﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSlot : MonoBehaviour {

    public List<string> validCards;
    public Card myCard;

    public void Update() {
        if (myCard) {
            myCard.transform.position = this.transform.position;
        }
    }


    public void Equip(Card card) {
        if (CanEquip(card) && !myCard) {
            myCard = card;
            myCard.transform.SetParent(this.transform);
            card.gameObject.transform.position = this.transform.position;
            card.gameObject.transform.rotation = Quaternion.identity;
        }
    }
    public void Unequip() {
        myCard.transform.SetParent(null);
        myCard = null;
    }

    public bool CanEquip(Card card) {
        if (validCards.Contains(card.id)) {
            return true;
        }
        return false;
    }

    private void OnMouseEnter() {
        if (!myCard) {
            //Sine all the cards that can use this
            foreach (Card card in Card.Cards) {
                if (CanEquip(card)) {
                    card.SetShining(true);
                }
            }
        }
    }

    private void OnMouseExit() {
        if (!myCard) {
            //Stop shining all the cards
            foreach (Card card in Card.Cards) {
                card.SetShining(false);
            }
        }
    }

}
