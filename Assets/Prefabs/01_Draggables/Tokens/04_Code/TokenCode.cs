﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenCode : Token {


    public override bool CanUse() {
        CardSlot[] slots = myHUD.GetCardSlots();
        if (slots[0].myCard) {
            return true;
        }
        return false;
    }

    private CardColor consumedColor;

    public override void Use() {
        if (CanUse()) {
            CardSlot[] slots = myHUD.GetCardSlots();
            //Store the consumed card color and destroy it
            Card consumedCard = slots[0].myCard;
            consumedColor = consumedCard.cardColor;
            myHUD.Close();
            Destroy(consumedCard.gameObject);
            base.Use();
        }
    }

    public override void OnComplete() {
        base.OnComplete();
        //Spawn a code card
        GameObject newCard = (GameObject)Instantiate(GM.gm.cardGameObjects[3], this.transform.position + Vector3.up * 0.5f, Quaternion.identity);
        newCard.GetComponent<Card>().SetColor(consumedColor);

    }

}
