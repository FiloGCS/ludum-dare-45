﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenTime : Token {

    public GameObject TimeUpPrefab;

    public override bool CanEquip(Card card) {
        return false;
    }

    public override void Use() {
        base.Use();
    }

    public override void Start() {
        base.Start();
        Use();
    }

    public override void OnComplete() {
        base.OnComplete();
        //GAME OVER
        GameObject ResultsPanel = (GameObject)Instantiate(TimeUpPrefab, Vector3.zero + Vector3.up * 0.33f, Quaternion.Euler(90, 0, 0));
        GM.timeScale = 0.0f;
    }
}
