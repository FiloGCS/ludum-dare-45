﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenUpload : Token {

    public GameObject UploadNotificationPrefab;

    public override bool CanUse() {
        CardSlot[] slots = myHUD.GetCardSlots();
        if (slots[0].myCard) {
            return true;
        }
        return false;
    }

    private GameObject gameCard;

    public override void Use() {
        if (CanUse()) {
            CardSlot[] slots = myHUD.GetCardSlots();
            gameCard = slots[0].myCard.gameObject;
            myHUD.Close();
            gameCard.SetActive(false);
            base.Use();
        }
    }

    public override void OnComplete() {
        base.OnComplete();
        //Call the endgame function


        GameObject ResultsPanel = (GameObject) Instantiate(UploadNotificationPrefab, Vector3.zero + Vector3.up * 0.33f,Quaternion.Euler(90,0,0));
        float mechanicScore = gameCard.GetComponent<CardGame>().mechanicPoints * 2.0f;
        float artScore = gameCard.GetComponent<CardGame>().artPoints * 2.0f;
        float codeScore = gameCard.GetComponent<CardGame>().codePoints * 2.0f;
        int acum = 0;
        foreach (int x in gameCard.GetComponent<CardGame>().colors) {
            acum += x;
        }
        ResultsPanel.GetComponent<UploadNotification>().gamePartsText.text = (acum).ToString("0");
        ResultsPanel.GetComponent<UploadNotification>().designScoreText.text = (mechanicScore).ToString("0.0");
        ResultsPanel.GetComponent<UploadNotification>().artScoreText.text = (artScore).ToString("0.0");
        ResultsPanel.GetComponent<UploadNotification>().codeScoreText.text = (codeScore).ToString("0.0");
        ResultsPanel.GetComponent<UploadNotification>().overallScoreText.text = ((codeScore+artScore+mechanicScore)/3.0f).ToString("0.0");
        GM.timeScale = 0.0f;
    }
}
