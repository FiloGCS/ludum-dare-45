﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class UploadNotification : MonoBehaviour {

    public TextMeshProUGUI gamePartsText;
    public TextMeshProUGUI designScoreText;
    public TextMeshProUGUI artScoreText;
    public TextMeshProUGUI codeScoreText;
    public TextMeshProUGUI overallScoreText;


    public void ReloadScene() {
        SceneManager.LoadScene(0);
    }

    public void QuitGame() {
        Application.Quit();
    }

}
