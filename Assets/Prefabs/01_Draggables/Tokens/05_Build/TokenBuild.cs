﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenBuild : Token {

    public override bool CanUse() {
        //Necesita tener una carta de los tipos MCA en el primer slot
        CardSlot[] slots = myHUD.GetCardSlots();
        if (slots[0].myCard) {
            return true;
        }
        return false;

    }

    private GameObject gameCard;

    public override void Use() {
        if (CanUse()) {
            CardSlot[] slots = myHUD.GetCardSlots();
            Card consumedCard = slots[0].myCard;

            //If there's a game card already
            if (slots[1].myCard) {
                gameCard = slots[1].myCard.gameObject;
            } else {
                //Spawn a new gameCard
                gameCard = (GameObject)Instantiate(GM.gm.cardGameObjects[4], this.transform.position + Vector3.up * 0.5f, Quaternion.identity);
            }
            //Add the points to the game card
            gameCard.GetComponent<CardGame>().AddCard(consumedCard);
            gameCard.gameObject.SetActive(false);

            //Destroy consumed card
            myHUD.Close();
            Destroy(consumedCard.gameObject);
            base.Use();
        }
    }

    public override void OnComplete() {
        base.OnComplete();
        //Show the game card
        gameCard.gameObject.SetActive(true);
        gameCard.transform.position = this.transform.position + Vector3.up * 0.5f;
    }


    public override void Equip(Card card) {
        base.Equip(card);
        
        CardSlot[] slots = myHUD.GetCardSlots();
        //Si falta por equipar el juego
        if (!slots[1].myCard) {
            //Busca el primero juego que encuentres y equipalo automáticamente
            foreach(Card c in Card.Cards) {
                if(c.id == "game") {
                    EquipGame(c);
                    break;
                }
            }
        }
    }

    public void EquipGame(Card card) {
        base.Equip(card);
    }
}
