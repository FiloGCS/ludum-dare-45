﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragManager : MonoBehaviour {

    public Draggable dragging;
    private Vector3 hitPoint;
    private bool waiting = false;
    Vector3 mouseDownPosition;

    // Update is called once per frame
    void Update() {

        //Cuando no hay nada esparando
        if (!waiting) {
            if (Input.GetMouseButtonUp(0) && dragging != null) {
                dragging.Drop();
                dragging = null;
            }
            if (Input.GetMouseButtonDown(0)) {
                Ray ray = GM.camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                LayerMask mask = LayerMask.GetMask("Tokens", "Cards");
                //Si es un draggable válido:
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask)) {
                    Draggable draggable = hit.collider.gameObject.GetComponent<Draggable>();
                    if (draggable) {
                        dragging = draggable;
                        hitPoint = hit.point;
                        waiting = true;
                        mouseDownPosition = Input.mousePosition;
                    }
                }
            }
        } else {
            //Si suelto el mouse, es un click
            if (Input.GetMouseButtonUp(0)) {
                waiting = false;
                dragging.Click();
            } else {
                //Si muevo el mouse, es un drag
                if (Vector3.Distance(mouseDownPosition, Input.mousePosition) > 10.0f) {
                    waiting = false;
                    dragging.Drag(hitPoint);
                }
            }
        }
    }
}
