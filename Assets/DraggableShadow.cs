﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableShadow : MonoBehaviour {
    void Update() {
        this.transform.position = Vector3.Scale(this.transform.position, new Vector3(1, 0, 1));
    }
}
